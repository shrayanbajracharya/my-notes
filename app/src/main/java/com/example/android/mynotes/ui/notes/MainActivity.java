package com.example.android.mynotes.ui.notes;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.example.android.mynotes.R;
import com.example.android.mynotes.data.Note;
import com.example.android.mynotes.ui.AddEditNoteActivity;
import com.example.android.mynotes.ui.ViewNoteActivity;
import com.example.android.mynotes.utils.NoteUtils;
import com.example.android.mynotes.utils.ToastUtils;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {

    private NoteViewModel noteViewModel;
    private RecyclerView notesRecyclerView;
    private NoteAdapter adapter = new NoteAdapter();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initRecyclerView();
        initViewModel();
        observeNotes();
        addNote();
        swipeActions();
        adapterItemClickListeners();
    }

    // Setting LayoutManager and Adapter to RecyclerView
    private void initRecyclerView() {
        notesRecyclerView = findViewById(R.id.notes_recycler_view);
        notesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        notesRecyclerView.setHasFixedSize(true);
        notesRecyclerView.setAdapter(adapter);
    }

    private void initViewModel() {
        noteViewModel = ViewModelProviders.of(this).get(NoteViewModel.class);
    }

    private void observeNotes() {
        noteViewModel.getAllNotes().observe(this, new Observer<List<Note>>() {

            @Override
            public void onChanged(List<Note> notes) {
                adapter.setNoteList((ArrayList<Note>) notes);
            }
        });
    }

    private void addNote() {
        FloatingActionButton fabAddNote = findViewById(R.id.fab_add_note);
        fabAddNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddEditNoteActivity.class);
                startActivityForResult(intent, NoteUtils.ADD_NOTE_REQUEST);
            }
        });
    }

    private void adapterItemClickListeners() {
        adapter.setOnItemClickListener(new NoteAdapter.OnItemClickListener() {
            @Override
            public void itemClick(Note note) {
                Intent intent = new Intent(MainActivity.this, ViewNoteActivity.class);

                intent.putExtra(NoteUtils.EXTRA_ID, note.getId());
                intent.putExtra(NoteUtils.EXTRA_TITLE, note.getTitle());
                intent.putExtra(NoteUtils.EXTRA_DESCRIPTION, note.getDescription());
                intent.putExtra(NoteUtils.EXTRA_PRIORITY, note.getPriority());

                startActivity(intent);
            }
        });
    }

    // Receiving data back from AddEditNoteActivity
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == NoteUtils.ADD_NOTE_REQUEST && resultCode == RESULT_OK) {
            if (data == null) {
                return;
            }

            String receivedTitle = data.getStringExtra(NoteUtils.EXTRA_TITLE);
            String receivedDescription = data.getStringExtra(NoteUtils.EXTRA_DESCRIPTION);
            int receivedPriority = data.getIntExtra(NoteUtils.EXTRA_PRIORITY, NoteUtils.DEFAULT_PRIORITY);

            Note addedNote = new Note(receivedTitle, receivedDescription, receivedPriority);
            noteViewModel.insert(addedNote);

            ToastUtils.showToast(getApplicationContext(), getString(R.string.note_saved));
        } else {
            ToastUtils.showToast(getApplicationContext(), getString(R.string.note_not_saved));
        }
    }

    private void swipeActions() {
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView,
                                  @NonNull RecyclerView.ViewHolder viewHolder,
                                  @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

                Note leftSwipedNote = adapter.getNoteAt(viewHolder.getAdapterPosition());
                int priority = leftSwipedNote.getPriority();

                String message;
                if (priority <= NoteUtils.MIN_PRIORITY) { //If Priority reaches 1 don't decrement
                    message = "Least Priority Reached";
                } else {
                    priority--;
                    message = "Priority Decreased";
                }

                leftSwipedNote.setPriority(priority);
                noteViewModel.update(leftSwipedNote);
                ToastUtils.showToast(MainActivity.this, message);
            }
        }).attachToRecyclerView(notesRecyclerView);

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView,
                                  @NonNull RecyclerView.ViewHolder viewHolder,
                                  @NonNull RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {

                Note rightSwipedNote = adapter.getNoteAt(viewHolder.getAdapterPosition());
                int priority = rightSwipedNote.getPriority();

                String message;
                if (priority >= NoteUtils.MAX_PRIORITY) { //If Priority reaches 10, don't increment
                    message = "Highest Priority Reached";
                } else {
                    priority++;
                    message = "Priority Increased";
                }

                rightSwipedNote.setPriority(priority);
                noteViewModel.update(rightSwipedNote);
                ToastUtils.showToast(MainActivity.this, message);
            }
        }).attachToRecyclerView(notesRecyclerView);
    }

    // Inflating menu layout in this activity
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    // Actions for item clicks in menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.delete_all_notes) {
            deleteAllNotes();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void deleteAllNotes() {
        noteViewModel.deleteAllNotes();
        ToastUtils.showToast(getApplicationContext(), getString(R.string.all_notes_deleted));
    }
}
