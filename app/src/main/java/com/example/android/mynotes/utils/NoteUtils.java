package com.example.android.mynotes.utils;

import android.content.Intent;

import com.example.android.mynotes.data.Note;

public final class NoteUtils {

    // Minimum and Maximum Priority
    public static final int MIN_PRIORITY = 1;
    public static final int MAX_PRIORITY = 5;

    // Default Values
    public static final int DEFAULT_PRIORITY = 1;
    public static final String DEFAULT_TITLE = "Default Title";
    public static final String DEFAULT_DESCRIPTION = "Default Description";

    // Request Codes
    public static final int ADD_NOTE_REQUEST = 1;
    public static final int EDIT_NOTE_REQUEST = 2;

    // Error Codes
    public static final int NO_ID = -1;

    // Keys for sending data
    public static final String EXTRA_ID =
            "com.example.android.mynotes.EXTRA_ID";
    public static final String EXTRA_TITLE =
            "com.example.android.mynotes.EXTRA_TITLE";
    public static final String EXTRA_DESCRIPTION = "" +
            "com.example.android.mynotes.EXTRA_DESCRIPTION";
    public static final String EXTRA_PRIORITY =
            "com.example.android.mynotes.EXTRA_PRIORITY";

    public static Note extractDataFromIntent(Intent intent) {

        int id = intent.getIntExtra(NoteUtils.EXTRA_ID, NoteUtils.NO_ID);
        String title = intent.getStringExtra(NoteUtils.EXTRA_TITLE);
        int priority = intent.getIntExtra(NoteUtils.EXTRA_PRIORITY, NoteUtils.DEFAULT_PRIORITY);
        String description = intent.getStringExtra(NoteUtils.EXTRA_DESCRIPTION);

        Note note = new Note(title, description, priority);
        note.setId(id);

        return note;
    }
}
