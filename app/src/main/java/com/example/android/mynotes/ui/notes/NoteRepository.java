package com.example.android.mynotes.ui.notes;

import android.app.Application;
import android.os.AsyncTask;

import com.example.android.mynotes.app.AppDatabase;
import com.example.android.mynotes.data.Note;
import com.example.android.mynotes.data.NoteDao;

import java.util.List;

import androidx.lifecycle.LiveData;

public class NoteRepository {

    private static NoteRepository instance;

    private static NoteDao noteDao;
    private static LiveData<List<Note>> allNotes;

    private NoteRepository() {
    }

    private NoteRepository(Application application) {
        AppDatabase database = AppDatabase.getInstance(application);
        noteDao = database.noteDao();
        allNotes = noteDao.getAllNotes();
    }

    public static NoteRepository getInstance(Application application) {
        if (instance == null) {
            instance = new NoteRepository(application);
            AppDatabase database = AppDatabase.getInstance(application);
            noteDao = database.noteDao();
            allNotes = noteDao.getAllNotes();
        }
        return instance;
    }

    /**
     * Using AsyncTask instance to insert notes (through repository)
     *
     * @param note is the note to be inserted into the database
     */
    public void insert(Note note) {
        new insertNoteAsyncTask(noteDao).execute(note);
    }

    /**
     * Using AsyncTask instance to update notes (through repository)
     *
     * @param note is the note to be updated in the database
     */
    public void update(Note note) {
        new updateNoteAsyncTask(noteDao).execute(note);
    }

    /**
     * Using AsyncTask instance to delete notes (through repository)
     *
     * @param note is the note to be deleted from the database
     */
    public void delete(Note note) {
        new deleteNoteAsyncTask(noteDao).execute(note);
    }

    /**
     * Using AsyncTask instance to delete all notes (through repository)
     */
    public void deleteAllNotes() {
        new deleteAllNotesAsyncTask(noteDao).execute();
    }

    /**
     * Using AsyncTask instance to get all notes (through repository)
     */
    public LiveData<List<Note>> getAllNotes() {
        return allNotes;
    }

    public static class insertNoteAsyncTask extends AsyncTask<Note, Void, Void> {

        private NoteDao noteDao;

        private insertNoteAsyncTask(NoteDao noteDao) {
            this.noteDao = noteDao;
        }

        @Override
        protected Void doInBackground(Note... notes) {

            noteDao.insert(notes[0]);
            return null;
        }
    }

    /**
     * Using AsyncTask instance to update specific note (through repository)
     */
    public static class updateNoteAsyncTask extends AsyncTask<Note, Void, Void> {

        private NoteDao noteDao;

        private updateNoteAsyncTask(NoteDao noteDao) {
            this.noteDao = noteDao;
        }

        @Override
        protected Void doInBackground(Note... notes) {

            noteDao.update(notes[0]);
            return null;
        }
    }

    /**
     * Using AsyncTask instance to delete a specific note (through repository)
     */
    public static class deleteNoteAsyncTask extends AsyncTask<Note, Void, Void> {

        private NoteDao noteDao;

        private deleteNoteAsyncTask(NoteDao noteDao) {
            this.noteDao = noteDao;
        }

        @Override
        protected Void doInBackground(Note... notes) {
            noteDao.delete(notes[0]);
            return null;
        }
    }

    /**
     * Using AsyncTask instance to delete all notes (through repository)
     */
    public static class deleteAllNotesAsyncTask extends AsyncTask<Void, Void, Void> {

        private NoteDao noteDao;

        private deleteAllNotesAsyncTask(NoteDao noteDao) {
            this.noteDao = noteDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            noteDao.deleteAllNotes();
            return null;
        }
    }
}
