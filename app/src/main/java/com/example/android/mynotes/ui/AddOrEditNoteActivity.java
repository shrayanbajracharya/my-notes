package com.example.android.mynotes.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.NumberPicker;

import com.example.android.mynotes.R;
import com.example.android.mynotes.utils.NoteUtils;
import com.example.android.mynotes.utils.ToastUtils;

import androidx.appcompat.app.AppCompatActivity;

public class AddEditNoteActivity extends AppCompatActivity {

    // Views of layout
    EditText etTitle;
    EditText etDescription;
    NumberPicker priorityPicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_note);

        mappingViews();
        setMinAndMaxValuesOfPriorityPicker();
        setCloseButtonInActionBar();
        Intent intent = getIntent();

        if (intent.hasExtra(NoteUtils.EXTRA_ID)) {
            setTitle(getString(R.string.edit_note));
            prepareViewsForEditMode(intent);
        } else {
            setTitle(getString(R.string.add_note));
        }
    }

    private void mappingViews() {
        etTitle = findViewById(R.id.edit_text_title);
        etDescription = findViewById(R.id.edit_text_description);
        priorityPicker = findViewById(R.id.priority_picker);
    }

    private void setMinAndMaxValuesOfPriorityPicker() {
        priorityPicker.setMinValue(NoteUtils.MIN_PRIORITY);
        priorityPicker.setMaxValue(NoteUtils.MAX_PRIORITY);
    }

    private void setCloseButtonInActionBar() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
        }
    }

    private void prepareViewsForEditMode(Intent intent) {
        etTitle.setText(intent.getStringExtra(NoteUtils.EXTRA_TITLE));
        etDescription.setText(intent.getStringExtra(NoteUtils.EXTRA_DESCRIPTION));
        priorityPicker.setValue(
                intent.getIntExtra(NoteUtils.EXTRA_PRIORITY, NoteUtils.DEFAULT_PRIORITY)
        );
    }

    // Inflating the menu layout to Menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.add_note_menu, menu);
        return true;
    }

    /**
     * Defines action of the items in the Menu
     *
     * @param item is the selected item
     * @return true after finishing operation
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_save_note) {
            saveNote();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void saveNote() {
        mappingViews();

        if (areInputsValid()) {
            ToastUtils.showToast(this, "Please enter both title and description");
            return;
        }
        Intent intent = prepareNoteForSendingItBackToViewNoteActivity();

        int id = getIntent().getIntExtra(NoteUtils.EXTRA_ID, NoteUtils.NO_ID);
        if (id != NoteUtils.NO_ID) {
            intent.putExtra(NoteUtils.EXTRA_ID, id);
        }

        setResult(RESULT_OK, intent);
        finish();
    }

    private boolean areInputsValid() {
        return !TextUtils.isEmpty(etTitle.getText().toString().trim())
                && !TextUtils.isEmpty(etDescription.getText().toString().trim());
    }

    private Intent prepareNoteForSendingItBackToViewNoteActivity() {
        // Intent to send data back to ViewNoteActivity
        Intent intent = new Intent();

        // Retrieving data from the input fields
        String title = etTitle.getText().toString().trim();
        String description = etDescription.getText().toString().trim();
        int priority = priorityPicker.getValue();

        intent.putExtra(NoteUtils.EXTRA_TITLE, title);
        intent.putExtra(NoteUtils.EXTRA_DESCRIPTION, description);
        intent.putExtra(NoteUtils.EXTRA_PRIORITY, priority);

        return intent;
    }
}
