package com.example.android.mynotes.utils;

import android.content.Context;
import android.widget.Toast;

public final class ToastUtils {

    private static Toast toast;

    public static void showToast(Context context, String message) {
        if (toast != null) { // Check if toast is being shown
            toast.cancel();
        }
        toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        toast.show();
    }
}



