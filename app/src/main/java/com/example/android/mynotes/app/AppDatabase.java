package com.example.android.mynotes.app;

import android.content.Context;
import android.os.AsyncTask;

import com.example.android.mynotes.data.Note;
import com.example.android.mynotes.data.NoteDao;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

@Database(entities = {Note.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase instance;
    public abstract NoteDao noteDao();

    /**
     * Synchronized to restrict multiple threads to run instance of the database
     *
     * @param context is the context of the application
     * @return singleton instance of database
     */
    public static synchronized AppDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(
                    context.getApplicationContext(), AppDatabase.class, "note_database")
                    .fallbackToDestructiveMigration()
                    .addCallback(roomCallback)
                    .build();
        }
        return instance;
    }

    /**
     * Callback is used to Populate the note list when the database is created
     * Placing dummy data in the newly created database
     */
    public static RoomDatabase.Callback roomCallback = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
            new PopulateDbAsyncTask(instance).execute();
        }
    };

    // AsyncTask used to populate the note list when database is first created
    private static class PopulateDbAsyncTask extends AsyncTask<Void, Void, Void> {

        private NoteDao noteDao;

        PopulateDbAsyncTask(AppDatabase database) {
            this.noteDao = database.noteDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            // Dummy data for populating database
            noteDao.insert(new Note("Title 1", "Description 1", 1));
            noteDao.insert(new Note("Title 2", "Description 2", 2));
            noteDao.insert(new Note("Title 3", "Description 3", 3));
            return null;
        }
    }
}
