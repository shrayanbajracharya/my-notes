package com.example.android.mynotes.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.android.mynotes.R;
import com.example.android.mynotes.data.Note;
import com.example.android.mynotes.ui.notes.MainActivity;
import com.example.android.mynotes.ui.notes.NoteViewModel;
import com.example.android.mynotes.utils.NoteUtils;
import com.example.android.mynotes.utils.ToastUtils;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

public class ViewNoteActivity extends AppCompatActivity {

    Note viewedNote;
    NoteViewModel noteViewModel;
    TextView viewNoteTitle;
    TextView viewNotePriority;
    TextView viewNoteDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_note);

        Intent intent = getIntent();
        if (intent == null) {
            return;
        }
        if (getSupportActionBar() == null) {
            return;
        }
        initViewModel();
        mappingViews();
        getSupportActionBar().setTitle(getString(R.string.view_note));
        viewedNote = NoteUtils.extractDataFromIntent(intent);
        displayData(viewedNote);
    }

    private void initViewModel() {
        noteViewModel = ViewModelProviders.of(this).get(NoteViewModel.class);
    }

    private void mappingViews() {
        viewNoteTitle = findViewById(R.id.view_note_title);
        viewNotePriority = findViewById(R.id.view_note_priority);
        viewNoteDescription = findViewById(R.id.view_note_description);
    }

    private void displayData(Note note) {
        viewNoteTitle.setText(note.getTitle());
        viewNotePriority.setText(String.valueOf(note.getPriority()));
        viewNoteDescription.setText(note.getDescription());
    }

    // Inflating menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.view_note_menu, menu);
        return true;
    }

    // Defining actions of the menu items
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit_note:
                editNote(viewedNote);
                return true;
            case R.id.action_delete_note:
                deleteNote(viewedNote);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void editNote(Note note) {

        Intent intent = new Intent(ViewNoteActivity.this, AddEditNoteActivity.class);

        intent.putExtra(NoteUtils.EXTRA_ID, note.getId());
        intent.putExtra(NoteUtils.EXTRA_TITLE, note.getTitle());
        intent.putExtra(NoteUtils.EXTRA_DESCRIPTION, note.getDescription());
        intent.putExtra(NoteUtils.EXTRA_PRIORITY, note.getPriority());

        startActivityForResult(intent, NoteUtils.EDIT_NOTE_REQUEST);
    }

    /**
     * Defines what action to be performed on the returned data from AddEditNoteActivity
     *
     * @param requestCode indicates which specifies what type of operation
     * @param resultCode  indicates the status of the result
     * @param data        is the Intent that contains data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == NoteUtils.EDIT_NOTE_REQUEST && resultCode == RESULT_OK) {
            if (data == null) {
                return;
            }
            int id = data.getIntExtra(NoteUtils.EXTRA_ID, NoteUtils.NO_ID);
            if (id == NoteUtils.NO_ID) {
                ToastUtils.showToast(getApplicationContext(), getString(R.string.note_cannot_be_updated));
                return;
            }
            Note note = NoteUtils.extractDataFromIntent(data);
            noteViewModel.update(note);
            ToastUtils.showToast(getApplicationContext(), getString(R.string.note_saved));
            displayData(note);
        } else {
            ToastUtils.showToast(getApplicationContext(), getString(R.string.note_not_saved));
        }
    }

    private void deleteNote(Note note) {
        noteViewModel.delete(note);
        ToastUtils.showToast(getApplicationContext(), getString(R.string.note_deleted));
        Intent intent = new Intent(ViewNoteActivity.this, MainActivity.class);
        startActivity(intent);
    }
}